// function myFunction() {
//     console.log("mi funcion");
// }


//Funcion anonima
let mi_funcion = function() {
    console.log("otra funcion");
}

let main = function(){
    my_button();
}

let my_button = function() {
    document.querySelector(".myButton input").setAttribute("onclick","dataRead()");
}

let dataRead = function() {
    console.log("Vamos a leer los datos del formulario");
    // console.log(
    //     document.querySelector("#nombre").value,
    //     document.querySelector("#apellido").value,
    //     document.querySelector("#email").value,
    //     document.querySelector("#pass").value,
    // );
    let my_objetc = {
        nombre : document.querySelector("#nombre").value,
        apellido : document.querySelector("#apellido").value,
        email : document.querySelector("#email").value,
        pass : document.querySelector("#pass").value
    };

    console.log(my_objetc);
    console.log(JSON.stringify(my_objetc));
    save(my_objetc);
    
}

let save = function(my_objetc) {
    localStorage.setItem("miInfo",JSON.stringify(my_objetc));
}

let read = function() {
    let my_storage = localStorage.getItem("miInfo");
    console.log(my_storage);
    let my_objetcJs = JSON.parse(my_storage);
    console.log(my_objetcJs);

    document.querySelector("#nombre").value = my_objetcJs.nombre;
    document.querySelector("#apellido").value = my_objetcJs.apellido;
    document.querySelector("#email").value = my_objetcJs.email;
    document.querySelector("#pass").value = my_objetcJs.pass;
}

main();

